function CelullarAutomata()
{
    
}

CelullarAutomata.prototype.createFromMaze = function(maze)
{
    this.arrays     = [];
    this.arrays[0]  = Utils.getFilledArray(maze.width, maze.height, 1);
    this.arrays[1]  = Utils.getFilledArray(maze.width, maze.height, 1);
    
    this.width  = maze.width;
    this.height = maze.height;
    
    for (var x = 0; x < this.width; x++)
    {
        for (var y = 0; y < this.height; y++)
        {
            this.arrays[0][Maze.getIndex(x, y, this.width, this.height)] = maze[Maze.getIndex(x, y, this.width, this.height)];
        }
    }
};

CelullarAutomata.prototype.evalCell = function(arr, x, y, width, height, value)
{
    var sum = 0;
    
    if (x > 0)
    {
        sum += arr[Maze.getIndex(x-1, y, width, height)] === value;
    }
    if (x < width-1)
    {
        sum += arr[Maze.getIndex(x+1, y, width, height)] === value;
    }
    if (y > 0)
    {
        sum += arr[Maze.getIndex(x, y-1, width, height)] === value;
    }
    if (y < height-1)
    {
        sum += arr[Maze.getIndex(x, y+1, width, height)] === value;
    }
    
    return sum;
};

CelullarAutomata.prototype.redNodeAround = function(arr, x, y, width, height)
{
    var sum = 0;
    
    if (x > 0)
    {
        if (arr[Maze.getIndex(x-1, y, width, height)] === 2)
        {
            return true;
        }
    }
    if (x < width-1)
    {
        if(arr[Maze.getIndex(x+1, y, width, height)] === 2)
        {
            return true;
        }
    }
    if (y > 0)
    {
        if (arr[Maze.getIndex(x, y-1, width, height)] === 2)
        {
            return true;
        }
    }
    if (y < height-1)
    {
        if (arr[Maze.getIndex(x, y+1, width, height)] === 2)
        {
            return true;
        }
    }
    
    return false;
};

CelullarAutomata.prototype.step = function()
{        
    var switchCounter = 0;
    
    for (var x = 0; x < this.width; x++)
    {
        for (var y = 0; y < this.height; y++)
        {
            var sum = this.evalCell(this.arrays[0], x, y, this.width, this.height, 0);
                        
            var corner = this.redNodeAround(this.arrays[0], x, y, this.width, this.height);
                                    
            if (this.arrays[0][Maze.getIndex(x, y, this.width, this.height)] === 0)
            {
                if (sum === 1 && !corner)
                {
                    this.arrays[1][Maze.getIndex(x, y, this.width, this.height)] = 1;
                    switchCounter++;
                }
                else
                {
                    this.arrays[1][Maze.getIndex(x, y, this.width, this.height)] = 0;
                }
            }
            else if (this.arrays[0][Maze.getIndex(x, y, this.width, this.height)] === 1)
            {
                this.arrays[1][Maze.getIndex(x, y, this.width, this.height)] = 1;
            }
            else
            {
                this.arrays[1][Maze.getIndex(x, y, this.width, this.height)] = this.arrays[0][Maze.getIndex(x, y, this.width, this.height)];
            }
        }
    }
    
    var tmp = this.arrays[0];
    this.arrays[0] = this.arrays[1];
    this.arrays[1] = tmp;
    
    return switchCounter;
};

CelullarAutomata.prototype.draw = function(canvas) 
{
    this.ctx = canvas === null ? null : canvas.getContext('2d');    
    if (this.ctx === null) return this;
    var width = this.width, height = this.height, ctx = this.ctx,
        scalex = Math.floor(ctx.canvas.width / width),  // cell width
        scaley = Math.floor(ctx.canvas.height / height); // cell height
        
    for (var y = 0; y < this.height; y++) {
        for (var x = 0; x < this.width; x++) {
            var index = Maze.getIndex(x, y, this.width, this.height);            
            var state = this.arrays[0][index];    
            
            switch (state)
            {
                case 0:
                    ctx.fillStyle = "#fff";
                    break;
                case 1:
                    ctx.fillStyle = "#000";
                    break;
                case 2:
                    ctx.fillStyle = "#f00";
                    break;
            }
            
            // render cell
            ctx.fillRect(scalex * x, scaley * y, scalex, scaley);
        }
    }
    return this;
};