Utils = {};

Utils.getFilledArray = function(width, height, value) {
    if (value === null) 
        value = 1;
    
    var array = new Uint32Array(width * height);
    
    for (var i = 0; i < array.length; i++) 
        array[i] = value;
    return array;
    
};

// search for nearest power of two and subtracts one from it
Utils.nearest = function(dim, scale) {
    var floor = Math.floor(dim / scale);
    // if it is even, subtract one, or else leave it
    return ((floor % 2) === 0) ? floor - 1 : floor;
};