var MazeGenerator = {
    /** @const */
    DIRS: [
        {x:  0, y: -1}, // down
        {x:  1, y:  0}, // right
        {x:  0, y:  1}, // up
        {x: -1, y:  0}  // left
    ],
    /**
     * @returns {Array} a copy of a random direction ordering
     */
    dirs: function() {
        return MazeGenerator.shuffle(MazeGenerator.DIRS.slice(0));
    }
};

MazeGenerator.shuffle = function(array) {
    // counter points right after last item
    var counter = array.length;
    // we want to loop through all array
    while (counter > 0) {
        // get index of random item in array
        var index = Math.floor(Math.random() * counter);
        // decrease counter (so at the beginning, it points to last item in arra)
        counter--;
        
        // swap random array item with item on counter position
        var tmp = array[counter];
        array[counter] = array[index];
        array[index] = tmp;
    }
    return array;
};

MazeGenerator.popRandom = function(array) {
    // get random item index
    var i = Math.floor(Math.random() * array.length);
    // if it points to last item in array
    if (i === array.length - 1) {
        // just return popped item
        return array.pop();
    } else {
        // else, store item from random index
        var element = array[i];
        // remove last item from array, and store it in place of random item
        // we must do a pop(), so call it but store result on previous random index
        array[i] = array.pop();
        // return random item
        return element;
    }
};

MazeGenerator.isInBounds = function(x, y, width, height) {
    return x >= 0 && x < width && y >= 0 && y < height;
};

MazeGenerator.generateByPrim = function(width, height) 
{
    var maze    = Utils.getFilledArray(width, height, 1);    
    
    maze.width  = width;
    maze.height = height;
    
    var wallItem = {wx : 1, wy : 0, cx : 2, cy: 0 };
    
    maze[Maze.getIndex(0, 0, width, height)] = 0;
    
    var walls = [ wallItem ];
    
    while (walls.length > 0)
    {
        wallItem = MazeGenerator.popRandom(walls);
        
        var cx = wallItem.cx, cy = wallItem.cy, wx = wallItem.wx, wy = wallItem.wy;
        
        var windex = Maze.getIndex(wx, wy, width, height );
        var cindex = Maze.getIndex(cx, cy, width, height );
                
        if (MazeGenerator.isInBounds(cx, cy, width, height) && maze[cindex] === 1)
        {                        
            maze[windex] = 0;
            maze[cindex] = 0;
            
            for (var i = 0; i < MazeGenerator.DIRS.length; i++)
            {
                var dir = MazeGenerator.DIRS[i];
                
                walls.push( { 
                    wx : cx + 1 * dir.x, 
                    wy : cy + 1 * dir.y, 
                    cx : cx + 2 * dir.x, 
                    cy : cy + 2 * dir.y
                } );
            }
        }
    }
    
    return maze;
};