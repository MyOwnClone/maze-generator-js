function MazeRenderer(width, height, maze, canvas) 
{
    this.width  = width;
    this.height = height;
    this.maze   = maze;
    this.ctx    = canvas === null ? null : canvas.getContext('2d');
}

MazeRenderer.prototype.draw = function() 
{
    if (this.ctx === null) return this;
    var width = this.width, height = this.height, ctx = this.ctx,
        sx = Math.floor(ctx.canvas.width / width),  // cell width
        sy = Math.floor(ctx.canvas.height / height); // cell height
        
    for (var y = 0; y < this.height; y++) {
        for (var x = 0; x < this.width; x++) {
            var index = Maze.getIndex(x, y, this.width, this.height);            
            var state = this.maze[index];    
            // set color
            ctx.fillStyle = state === 1 ? "#000" : "#fff";
            // render cell
            ctx.fillRect(sx * x, sy * y, sx, sy);
        }
    }
    return this;
};