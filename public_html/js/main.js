// set same random seed each start, making generated random numbers repeat every start
Math.seedrandom("");

var maze        = null;
var canvas      = null;
var scale       = 5;
var width, height;
var automata    = null;
var interval    = 1000/60;

function init() {
    canvas = $('#display')[0];
    
    // adjust size of maze
    width   = Utils.nearest(canvas.width, scale);
    height  = Utils.nearest(canvas.height, scale);

    // generate maze by using prims algorithm
    maze = MazeGenerator.generateByPrim(width, height);
    maze[Maze.getIndex(0, 0, width, height)] = 2;
    maze[Maze.getIndex(width-1, height-1, width, height)] = 2;
        
    // create CA from Maze    
    automata = new CelullarAutomata();
    automata.createFromMaze(maze);
    
    render();
    window.setInterval("render()", interval);
}

function render()
{       
    if (automata !== null)
    {
        // do a CA step
        var switchCounter = automata.step();
        // render CA
        if (switchCounter > 0)
        {
            automata.draw(canvas);
        }
        else
        {
            var ctx = canvas === null ? null : canvas.getContext('2d');
            
            if (ctx !== null)
            {
                ctx.fillStyle="#f00";
                ctx.fillText("Path found!", 100, 100);
            }
        }
    }
}

$(document).ready(init);
